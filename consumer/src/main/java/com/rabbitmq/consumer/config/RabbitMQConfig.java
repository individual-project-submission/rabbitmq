package com.rabbitmq.consumer.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 流星
 */
@Configuration
public class RabbitMQConfig {
    /**
     * 声明交换机
     */
    public static final String EXCHANGE_TOPICS_INFORM="exchange_topics_inform";
    /**
     * 声明QUEUE_INFORM_EMAIL队列
     */
    public static final String QUEUE_INFORM_EMAIL = "queue_inform_email";
    /**
     * 声明QUEUE_INFORM_SMS队列
     */
    public static final String QUEUE_INFORM_SMS = "queue_inform_sms";
    /**
     * ROUTINGKEY_EMAIL队列绑定交换机，指定routingKey
     */
    public static final String ROUTINGKEY_EMAIL="inform.#.email.#";
    /**
     * ROUTINGKEY_SMS队列绑定交换机，指定routingKey
     */
    public static final String ROUTINGKEY_SMS="inform.#.sms.#";

    @Bean(EXCHANGE_TOPICS_INFORM)
    public Exchange EXCHANGE_TOPICS_INFORM(){
        //durable(true) 持久化，mq重启之后交换机还在
        return ExchangeBuilder.topicExchange(EXCHANGE_TOPICS_INFORM).durable(true).build();
    }

    @Bean(QUEUE_INFORM_EMAIL)
    public Queue QUEUE_INFORM_EMAIL(){
        return new Queue(QUEUE_INFORM_EMAIL);
    }

    @Bean(QUEUE_INFORM_SMS)
    public Queue QUEUE_INFORM_SMS(){
        return new Queue(QUEUE_INFORM_SMS);
    }

    @Bean
    public Binding BINDING_QUEUE_INFORM_EMAIL(@Qualifier(QUEUE_INFORM_EMAIL) Queue queue,
                                              @Qualifier(EXCHANGE_TOPICS_INFORM) Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(ROUTINGKEY_EMAIL).noargs();
    }

    @Bean
    public Binding BINDING_ROUTINGKEY_SMS(@Qualifier(QUEUE_INFORM_SMS) Queue queue,
                                          @Qualifier(EXCHANGE_TOPICS_INFORM) Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(ROUTINGKEY_SMS).noargs();
    }
}
