package com.rabbitmq.producer.controller;

import com.rabbitmq.producer.config.RabbitMQConfig;
import org.junit.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * description TODO
 *
 * @author 流星
 * @date 2022/4/30 15:38
 */
@RestController
@RequestMapping
public class MainController {
    @Autowired
    RabbitTemplate rabbitTemplate;

    @RequestMapping("rabbit")
    public void producerTest() {
        //使用rabbitTemplate发送消息
        String message = "send email message to user";
        /**
         * 参数：
         * 1、交换机名称
         * 2、routingKey
         * 3、消息内容
         */
        rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE_TOPICS_INFORM, "inform.email", message);
    }
}
