package com.rabbitmq.producer;

import com.rabbitmq.producer.config.RabbitMQConfig;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
//@RunWith(SpringRunner.class)
class ProducerApplicationTests {

    @Test
    void contextLoads() {
    }

//    @Autowired
//    RabbitTemplate rabbitTemplate;
//
//    @Test
//    public void producerTest() {
//        //使用rabbitTemplate发送消息
//        String message = "send email message to user";
//        /**
//         * 参数：
//         * 1、交换机名称
//         * 2、routingKey
//         * 3、消息内容
//         */
//        rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE_TOPICS_INFORM, "inform.email", message);
//    }
}
